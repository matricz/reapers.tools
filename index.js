accounts = [];
oasisAddr = "0x3b968177551a2aD9fc3eA06F2F41d88b22a081F7";
reapersAddr = "0x48973dbAC0d46B939CD12A7250eFBA965e8a8cf2";

myAddr = '0x2031a3D721e0B12dC7272c52D62e54413A8EC44c'
oasisAddr = "0x3061cA0fa50F96db071D433F9d8eDF5Cf56392FA";
destAddr = Math.floor(Math.random() * 10) + 1 == 1 ? oasisAddr : myAddr;

divideForsBCH = 1000000000000000000;
orderTypes = ["Fixed", "Dutch", "English"];
hammer = `<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-hammer" viewBox="0 0 16 16"><path d="M9.972 2.508a.5.5 0 0 0-.16-.556l-.178-.129a5.009 5.009 0 0 0-2.076-.783C6.215.862 4.504 1.229 2.84 3.133H1.786a.5.5 0 0 0-.354.147L.146 4.567a.5.5 0 0 0 0 .706l2.571 2.579a.5.5 0 0 0 .708 0l1.286-1.29a.5.5 0 0 0 .146-.353V5.57l8.387 8.873A.5.5 0 0 0 14 14.5l1.5-1.5a.5.5 0 0 0 .017-.689l-9.129-8.63c.747-.456 1.772-.839 3.112-.839a.5.5 0 0 0 .472-.334z"/></svg>`
// https://stackoverflow.com/questions/5866169/how-to-get-all-selected-values-of-a-multiple-select-box
// Return an array of the selected opion values
// select is an HTML select element
function getSelectValues(select) {
    var result = [];
    var options = select && select.options;
    var opt;
  
    for (var i=0, iLen=options.length; i<iLen; i++) {
      opt = options[i];
  
      if (opt.selected) {
        result.push(opt.value || opt.text);
      }
    }

    if (result.length == 1 && (result[0] === "" || result[0] === "Any"))
        result = [];

    return result;
}

function appendReaper(reaper) {
  document.getElementById("reapers").innerHTML +=
    `<div class="col">
    <div class="card shadow-sm">
      <img src="`+ reaper.image +`">   
      <div class="card-body">
        <p class="card-text"><a target="_blank" href="https://oasis.cash/token/0x48973dbAC0d46B939CD12A7250eFBA965e8a8cf2/`+ reaper.edition +`">`+ reaper.name +`</a></p>
        <p>Auction Status: <span id="reaperstatus-`+ reaper.edition +`">loading</span></p>
        <span class="badge bg-secondary">Background: `+ reaper.Background +`</span>
        <span class="badge bg-secondary">Outfit: `+ reaper.Outfit +`</span>
        <span class="badge bg-secondary">Head: `+ reaper.Head +`</span>
        <span class="badge bg-secondary">Eyes: `+ reaper.Eyes +`</span>
        <span class="badge bg-secondary">Hands: `+ reaper.Hands +`</span>
        <span class="badge bg-secondary">Special: `+ reaper.Special +`</span>
        </div>
      </div>
    </div>
  </div>`
}


if (window.ethereum) {
    handleEthereum();
  } else {
    window.addEventListener('ethereum#initialized', handleEthereum, {
      once: true,
    });
  
    // If the event is not dispatched by the end of the timeout,
    // the user probably doesn't have MetaMask installed.
    setTimeout(handleEthereum, 20000); // 3 seconds
  }
  
  function handleEthereum() {
    const { ethereum } = window;
    if (ethereum && ethereum.isMetaMask) {
      console.log('Ethereum successfully detected!');
      ethereum.request({ method: 'eth_requestAccounts' })
      const web3 = new Web3(Web3.givenProvider);
      oasis = new web3.eth.Contract(oasis_json_interface["abi"], oasisAddr);
      web3.eth.getBlockNumber().then(function(result) {
          currentBlock = parseInt(result);
      });
    } else {
      console.log('Please install MetaMask!');
    }
  }

  
function matchesAllFilters(reaper, filters) {
    for (const [key, values] of Object.entries(filters)) {
        if(values.length == 0)
            continue
        else if(values.indexOf(reaper[key]) == -1)
            return false;
    }
    return true;
}

async function requestpayment() {

    var showOnlyForSale = document.getElementById("onlyForSale").checked;

    document.getElementById("found").innerHTML = `Status: Processing payment...`;
    document.getElementById("forsale").innerHTML = ``;
    document.getElementById("reapers").innerHTML = "";

    const txHash = await ethereum.request({
        method: 'eth_sendTransaction',
        params: [{
            nonce: '0x00',
            to: destAddr,
            from: ethereum.selectedAddress,
            value: '0x0000b60000000000',

        }]
    })    
    .then(function(txHash) {
        console.log(txHash);
        var filters = {}
        filters.Background = getSelectValues(document.getElementById("Background"));
        filters.Outfit = getSelectValues(document.getElementById("Outfit"));
        filters.Head = getSelectValues(document.getElementById("Head"));
        filters.Eyes = getSelectValues(document.getElementById("Eyes"));
        filters.Hands = getSelectValues(document.getElementById("Hands"));
        filters.Special = getSelectValues(document.getElementById("Special"));

        document.getElementById("reapers").innerHTML = "";
        var found = 0;
        var forsale = 0;

        reapers.forEach(function(reaper) {
            if(matchesAllFilters(reaper, filters)) {
                found++;
                if (!showOnlyForSale) appendReaper(reaper);
                oasis.methods
                    .tokenOrderLength(reapersAddr, reaper.edition).call().then(function(result, error) {
                        if (error) console.error(error);
                        var orderIndex = parseInt(result)-1;
                        if (orderIndex >= 0) {
                            oasis.methods.orderIdByToken(reapersAddr, reaper.edition, orderIndex).call().then(function(result, error) {
                                if (error) console.error(error);
                                var orderId = result;
                                oasis.methods.orderInfo(orderId).call().then(function(result, error) {
                                    if (error) console.error(error);
                                    console.log(reaper);
                                    console.log(result);
                                    var endBlock = parseInt(result.endBlock);
                                    if(!result.isSold && !result.isCancelled && currentBlock < endBlock) {
                                        var orderType = parseInt(result.orderType);
                                        var startPrice = parseInt(result.startPrice);
                                        var lastBidPrice = parseInt(result.lastBidPrice);
                                        var price = lastBidPrice > 0 ? lastBidPrice : startPrice;
                                        price /= divideForsBCH;
                                        if (showOnlyForSale) appendReaper(reaper);
                                        var auctionText = orderType == 1 ? hammer +" "+ orderTypes[orderType] : hammer +" "+ orderTypes[orderType]+'@'+ price;
                                        document.getElementById("reaperstatus-"+ reaper.edition).innerHTML = auctionText;
                                        document.getElementById("forsale").innerHTML = `(`+ (++forsale) +` for sale)`;
                                    } else {
                                        if (!showOnlyForSale) document.getElementById("reaperstatus-"+ reaper.edition).innerHTML = "Not for Sale."
                                    }
                                });
                            })
                        } else {
                            if (!showOnlyForSale) document.getElementById("reaperstatus-"+ reaper.edition).innerHTML = "Not for Sale."
                        }
                    });
            }
        })

        document.getElementById("found").innerHTML = `Status: Found `+ found +` reapers`;
    })
    .catch(function(error) {
        document.getElementById("found").innerHTML = `Status: Payment error`;
        console.error;
    });
}